object Form1: TForm1
  Left = 258
  Top = 131
  Width = 719
  Height = 480
  Caption = 'File Monitor'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 703
    Height = 441
    Align = alClient
    TabOrder = 0
  end
  object SHChangeNotify1: TSHChangeNotify
    OnAssocChanged = SHChangeNotify1AssocChanged
    OnAttributes = SHChangeNotify1Attributes
    OnCreate = SHChangeNotify1Create
    OnDelete = SHChangeNotify1Delete
    OnDriveAdd = SHChangeNotify1DriveAdd
    OnDriveAddGUI = SHChangeNotify1DriveAddGUI
    OnDriveRemoved = SHChangeNotify1DriveRemoved
    OnMediaInserted = SHChangeNotify1MediaInserted
    OnMediaRemoved = SHChangeNotify1MediaRemoved
    OnMkDir = SHChangeNotify1MkDir
    OnNetShare = SHChangeNotify1NetShare
    OnNetUnshare = SHChangeNotify1NetUnshare
    OnRenameFolder = SHChangeNotify1RenameFolder
    OnRenameItem = SHChangeNotify1RenameItem
    OnRmDir = SHChangeNotify1RmDir
    OnServerDisconnect = SHChangeNotify1ServerDisconnect
    OnUpdateDir = SHChangeNotify1UpdateDir
    OnUpdateImage = SHChangeNotify1UpdateImage
    OnUpdateItem = SHChangeNotify1UpdateItem
    Left = 632
    Top = 32
  end
end
