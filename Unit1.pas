unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, SHChangeNotify;

type
  TForm1 = class(TForm)
    SHChangeNotify1: TSHChangeNotify;
    Memo1: TMemo;
    procedure SHChangeNotify1Create(Sender: TObject; Flags: Cardinal;
      Path1: String);
    procedure FormCreate(Sender: TObject);
    procedure SHChangeNotify1AssocChanged(Sender: TObject; Flags: Cardinal;
      Path1, Path2: String);
    procedure SHChangeNotify1Attributes(Sender: TObject; Flags: Cardinal;
      Path1: String);
    procedure SHChangeNotify1Delete(Sender: TObject; Flags: Cardinal;
      Path1: String);
    procedure SHChangeNotify1MkDir(Sender: TObject; Flags: Cardinal;
      Path1: String);
    procedure SHChangeNotify1RenameFolder(Sender: TObject; Flags: Cardinal;
      Path1, Path2: String);
    procedure SHChangeNotify1RenameItem(Sender: TObject; Flags: Cardinal;
      Path1, Path2: String);
    procedure SHChangeNotify1RmDir(Sender: TObject; Flags: Cardinal;
      Path1: String);
    procedure SHChangeNotify1UpdateDir(Sender: TObject; Flags: Cardinal;
      Path1: String);
    procedure SHChangeNotify1UpdateImage(Sender: TObject; Flags: Cardinal;
      Path1: String);
    procedure SHChangeNotify1UpdateItem(Sender: TObject; Flags: Cardinal;
      Path1: String);
    procedure SHChangeNotify1DriveAdd(Sender: TObject; Flags: Cardinal;
      Path1: String);
    procedure SHChangeNotify1DriveAddGUI(Sender: TObject; Flags: Cardinal;
      Path1: String);
    procedure SHChangeNotify1DriveRemoved(Sender: TObject; Flags: Cardinal;
      Path1: String);
    procedure SHChangeNotify1MediaInserted(Sender: TObject;
      Flags: Cardinal; Path1: String);
    procedure SHChangeNotify1NetShare(Sender: TObject; Flags: Cardinal;
      Path1: String);
    procedure SHChangeNotify1NetUnshare(Sender: TObject; Flags: Cardinal;
      Path1: String);
    procedure SHChangeNotify1MediaRemoved(Sender: TObject; Flags: Cardinal;
      Path1: String);
    procedure SHChangeNotify1ServerDisconnect(Sender: TObject;
      Flags: Cardinal; Path1: String);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Log(sText:string);
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.SHChangeNotify1Create(Sender: TObject; Flags: Cardinal;
  Path1: String);
begin
  Log('Create : ' +Path1);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  SHChangeNotify1.Execute;
end;

procedure TForm1.Log(sText: string);
begin
    Memo1.Lines.Add(sText);
end;

procedure TForm1.SHChangeNotify1AssocChanged(Sender: TObject;
  Flags: Cardinal; Path1, Path2: String);
begin
 Log('Changed: ' +Path1 + ' >> ' + Path2);
end;

procedure TForm1.SHChangeNotify1Attributes(Sender: TObject;
  Flags: Cardinal; Path1: String);
begin
 Log('Attributes: ' +Path1  );
end;

procedure TForm1.SHChangeNotify1Delete(Sender: TObject; Flags: Cardinal;
  Path1: String);
begin
 Log('Delete: ' +Path1  );
end;

procedure TForm1.SHChangeNotify1MkDir(Sender: TObject; Flags: Cardinal;
  Path1: String);
begin
 Log('MkDIR: ' +Path1  );
end;

procedure TForm1.SHChangeNotify1RenameFolder(Sender: TObject;
  Flags: Cardinal; Path1, Path2: String);
begin
 Log('ReNameDir: ' +Path1 + ' >> ' + Path2);
end;

procedure TForm1.SHChangeNotify1RenameItem(Sender: TObject;
  Flags: Cardinal; Path1, Path2: String);
begin
 Log('ReName : ' +Path1 + ' >> ' + Path2);
end;

procedure TForm1.SHChangeNotify1RmDir(Sender: TObject; Flags: Cardinal;
  Path1: String);
begin
 Log('RmDir : ' +Path1  );
end;

procedure TForm1.SHChangeNotify1UpdateDir(Sender: TObject; Flags: Cardinal;
  Path1: String);
begin
 Log('Update : ' +Path1  );
end;

procedure TForm1.SHChangeNotify1UpdateImage(Sender: TObject;
  Flags: Cardinal; Path1: String);
begin
 Log('UpdateImage : ' +Path1  );
end;

procedure TForm1.SHChangeNotify1UpdateItem(Sender: TObject;
  Flags: Cardinal; Path1: String);
begin
 Log('UpdateItem : ' + Path1  );
end;

procedure TForm1.SHChangeNotify1DriveAdd(Sender: TObject; Flags: Cardinal;
  Path1: String);
begin
 Log('DriverAdd : ' + Path1  );
end;

procedure TForm1.SHChangeNotify1DriveAddGUI(Sender: TObject;
  Flags: Cardinal; Path1: String);
begin
 Log('DriverAddGUI : ' + Path1  );
end;

procedure TForm1.SHChangeNotify1DriveRemoved(Sender: TObject;
  Flags: Cardinal; Path1: String);
begin
 Log('DriverRemove : ' + Path1  );
end;

procedure TForm1.SHChangeNotify1MediaInserted(Sender: TObject;
  Flags: Cardinal; Path1: String);
begin
 Log('MediaInsert : ' + Path1  );
end;

procedure TForm1.SHChangeNotify1NetShare(Sender: TObject; Flags: Cardinal;
  Path1: String);
begin
 Log('NetShare : ' + Path1  );
end;

procedure TForm1.SHChangeNotify1NetUnshare(Sender: TObject;
  Flags: Cardinal; Path1: String);
begin
 Log('NetUnShare : ' + Path1  );
end;

procedure TForm1.SHChangeNotify1MediaRemoved(Sender: TObject;
  Flags: Cardinal; Path1: String);
begin
 Log('MediaRemoved: ' + Path1  );
end;

procedure TForm1.SHChangeNotify1ServerDisconnect(Sender: TObject;
  Flags: Cardinal; Path1: String);
begin
 Log('ServerDisConnect : ' + Path1  );
end;

end.
